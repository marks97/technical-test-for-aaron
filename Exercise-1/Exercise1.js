'use strict';

let OP = '(';
let CP = ')';

function is_valid(str) {
    var open = 0;
    for (let c of str) {
        if (c === OP) {
            open++;
        } else if (c === CP) {
            if (open > 0 ) {
                open--;
            } else {
                return false;
            }
        }
    }

    return open === 0;
}