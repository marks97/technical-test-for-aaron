def main():
    test = 'ligar es ser agil'
    trimmed_test = test.replace(' ', '')
    print(is_palindrome(trimmed_test))


def is_palindrome(input):
    if len(input) == 1:
        return True
    elif len(input) == 2:
        return True if input[0] == input[1] else False
    else:
        return input[0] == input[len(input) - 1] and is_palindrome(input[1:len(input) - 1])


if __name__ == "__main__":
    main()
